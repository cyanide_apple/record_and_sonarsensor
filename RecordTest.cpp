/****************************
*Record.cpp                 *
*****************************
*author : Tugce Dinc        *
*IDE : Visual Studio 2015   *
*Last Update: 16/12/2018    *
*****************************/
#include<iostream>
#include"Record.h"
using namespace std;
void test_record_with_operator()//<< ve >> operatorleri kullan�larak yap�ld�
{
	fstream dosya("output.txt", ios_base::out);// mutlaka olmal� output.txt
	dosya.close();

	string fileName="input.txt",readed;//m�tlaka olmal� input.txt
	
	Record write;
	Record yeni;

	yeni.setFileName(fileName);
	write.setFileName("output.txt");

	if (yeni.openFile()&&write.openFile())
	{
		while (1)
		{
			yeni >> readed;//dosyadan sat�r okur.
			if (readed != "")
			{
				write << readed;//yeni sat�ra veriyi yazar.
			}
			else
				break;
			readed = "";//readed de�i�kenini s�f�rlar.
		}
	}

	yeni.closeFile();//okunacak dosyay� kapat�r.
	write.closeFile();//yaz�lacak dosyay� kapat�r.
}

void test_record_without_operator()//<< ve >> operatorleri kullan�larak yap�ld�
{
	fstream dosya("output1.txt", ios_base::out);// mutlaka olmal� output.txt
	dosya.close();

	string fileName = "input.txt", readed;//m�tlaka olmal� input.txt

	Record write;
	Record yeni;

	yeni.setFileName(fileName);
	write.setFileName("output1.txt");

	if (yeni.openFile() && write.openFile())
	{
		while (1)
		{
			readed = yeni.readLine();//dosyadan sat�r okur.
			if (readed != "")
			{
				write.writeLine(readed);//yeni sat�ra veriyi yazar.
			}
			else
				break;
			readed = "";//readed de�i�kenini s�f�rlar.
		}
	}

	yeni.closeFile();//okunacak dosyay� kapat�r.
	write.closeFile();//yaz�lacak dosyay� kapat�r.
}

int main()
{
	test_record_with_operator();
	test_record_without_operator();
	return 0;
}