#include<iostream>
#include"PioneerRobotAPI.h"
#include"SonarSensor.h"
using namespace std;

void test_sonar_sensor()
{

	PioneerRobotAPI *robot = new PioneerRobotAPI();
	SonarSensor sonarsensor(robot);
	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return;
	}
	while (1)
	{
		robot->moveRobot(100);//robot do�rusal hareket ediyor
		int choice;
		cout << "Please, choose one direction" << endl
			<< "[1]\tLeft" << endl
			<< "[2]\tRight" << endl
			<< "[3]\tForvard" << endl;

		cin >> choice;
		switch (choice)
		{
		case 1:
			robot->turnRobot(PioneerRobotAPI::DIRECTION::left);
			break;
		case 2:
			robot->turnRobot(PioneerRobotAPI::DIRECTION::right);
			break;
		case 3:
			robot->turnRobot(PioneerRobotAPI::DIRECTION::forward);
			break;
		default:
			break;
		}
		//robot->turnRobot(PioneerRobotAPI::DIRECTION::left);
		Sleep(100);


		cout << "\n Sensorlerin Aci degerleri\n";
		for (int i = 0; i < 16; i++)
		{
			cout << i << ". sensor\t: " << sonarsensor.getAngle(i) << endl;
		}
		cout << "\n Sensorlerin mesafe degerleri\n";
		for (int i = 0; i < 16; i++)
		{
			cout << i << ". sensor\t: " << sonarsensor.getRange(i) << endl;
			//cout << i << ". sensor\t: " << sonarsensor[i] << endl;//kullan�lmasade olur ��nk� ayn� i�lem �st sat�rda da yap�l�yor.

		}
		int max=0;
		int min=0;
		cout << "max sensor : " << sonarsensor.getMax(max) << "  indis  " << max << endl;
		cout << "min sensor : " << sonarsensor.getMin(min) << "  indis  " << min << endl;
		float angle = 0;
		cout << "\n" << sonarsensor.getClosestRange(-170, 170, angle) << "  " << angle << "\n";

		system("pause");//her ad�mda sensorleri g�rebilmek i�in
	}
}
int main()
{
	test_sonar_sensor();
	return 0;
}