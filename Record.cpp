/****************************
*Record.h                   *
*****************************
*author : Tugce Dinc        *
*IDE : Visual Studio 2015   *
*Last Update: 20/12/2018    *
*****************************/
#include "Record.h"
using namespace std;

Record::Record()
{
}


Record::~Record()
{
}

bool Record::openFile()
{
	if (file->is_open() == false)
	{
		file->open(this->fileName, ios_base::in | ios_base::out);
		return true;
	}
	return false;
}

bool Record::closeFile()
{
	if (file->is_open() == true)
	{
		this->file->close();
		return true;
	}
	return false;
}

void Record::setFileName(string name)
{
	this->fileName = name;
	this->file = new fstream();
}

string Record::readLine()
{
	string str = "";
	if (this->file->is_open() == true && file->eof() == false)
	{
		*file >> str;
	}
	return str;
}

bool Record::writeLine(string str)
{
	if (this->file->is_open() == true)
	{
		*file << str << "\n";
		return true;
	}
	return false;
}

Record & operator >> (Record & in, string  &c1)
{
	if (in.file->is_open() == true)
	{
		getline(*in.file, c1);
	}
	return in;
}

Record & operator<<(Record & in, string c1)
{
	if (in.file->is_open() == true)
	{
		*in.file << c1;
	}
	return in;
}
