/****************************
*SonarSensor.cpp            *
*****************************
*author : Tugce Dinc        *
*IDE : Visual Studio 2015   *
*Last Update: 16/12/2018    *
*****************************/
#include "SonarSensor.h"



SonarSensor::SonarSensor(PioneerRobotAPI *robotAPi)
{
	robotAPI = robotAPi;
	robotAPI->getSonarRange(ranges);
}


SonarSensor::~SonarSensor()
{
	//destructor.
}

float SonarSensor::getRange(int index)
{
	robotAPI->getSonarRange(ranges);
	return ranges[index];
}

float SonarSensor::getMax(int & index)
{
	robotAPI->getSonarRange(ranges);
	float value = ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (ranges[i] > value)
		{
			value = ranges[i];
			index = i;
		}
	}
	return value;
}

float SonarSensor::getMin(int & index)
{
	robotAPI->getSonarRange(ranges);
	float value = ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (ranges[i] < value)
		{
			value = ranges[i];
			index = i;
		}
	}
	return value;
}

void SonarSensor::updateSensor(float ranges[])
{
	robotAPI->getSonarRange(ranges);
	for (int i = 0; i < 16; i++)
	{
		ranges[i] = this->ranges[i];
	}
}

float SonarSensor::operator[](int i)
{
	robotAPI->getSonarRange(ranges);
	return ranges[i];
}

float SonarSensor::getAngle(int index)
{
	robotAPI->getSonarRange(ranges);
	float angels[16] = { 90,50,30,10,-10,-30,-50,-90,-90,-130,-150,-170,170,150,130,90 };
	if (index > -1 && index < 16)
	{
		return angels[index];
	}
	return -1;
}

float SonarSensor::getClosestRange(float startAngle, float endAngle, float & angle)
{
	//robotAPI->getSonarRange(ranges);
	float angels[16] = { 90,50,30,10,-10,-30,-50,-90,-90,-130,-150,-170,170,150,130,90 };
	int start = 10 * ((int)(startAngle / 10)), end = 10 * ((int)(endAngle / 10));
	int startindex, endindex;

	for (int i = 0; i < 16; i++)
	{
		if (angels[i] == start)
		{
			startindex = i;
		}
		if (angels[i] == end)
		{
			endindex = i;
		}
	}
	int i = startindex;
	int shortest = ranges[i];
	while (1)
	{
		if (ranges[i] < shortest)
		{
			shortest = ranges[i];
			angle = angels[i];
		}
		if (i == endindex)
		{
			if (ranges[i] < shortest)
			{
				shortest = ranges[i];
				angle = angels[i];
			}
			return shortest;
		}
		if (i == 0)
		{
			i = 15;
		}
		else
			i--;
	}
	angle = -1;
	return 0;
}
