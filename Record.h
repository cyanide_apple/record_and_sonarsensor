/****************************
*Record.h                   *
*****************************
*author : Tugce Dinc        *
*IDE : Visual Studio 2015   *
*Last Update: 20/12/2018    *
*****************************/
#ifndef Record_h
#define Record_h
#include<string>
#include<fstream>
using namespace std;
/**
  *\author TUGCE DINC
  *\brief This class operates reading from file or writing in file.
  */
class Record
{ 
	/**
	  *\brief Name of file.
	  */
	string fileName;
	/**
	  *\brief File
	  */
	fstream *file;
public:
	/**
	  *\brief  It reads the string, which given as a parameter, from file.
	  *\param in                            : c1 is read from this object's file.
	  *\param &c1                            : given string 
	  */
	friend Record& operator >> (Record& in, string &c1);
	/**
	  *\brief  It writes the string, which given as a parameter, in new line.
	  *\param in                            : c1 is written in this object's file.
	  *\param c1                            : given string 
	  */
	friend Record & operator << (Record & in, string  c1);
	/**
	  *\brief Constructor.
	  */
	Record();
	/**
	  *\brief Destructor.
	  */
	~Record();
	/**
	  *\brief It opens the file.
	  *\return The correction of this situation
	  */
	bool openFile();
	/**
	  *\brief It opens the file.
	  *\return The correction of this situation
	  */
	bool closeFile();
	/**
	  *\brief It assigns "filename" .
	  *\param index                         : given filename.
	  */
	void setFileName(string name);
	/**
	  *\brief It reads a line from this file.
	  *\return It resturns the string which readed from file.
	  */
	string readLine();
	/**
	  *\brief  It writes the string, which given as a parameter, in new line.
	  *\param str                           : given string 
	  *\return The correction of this situation
	  */
	bool writeLine(string str);
};

#endif // !Record_h