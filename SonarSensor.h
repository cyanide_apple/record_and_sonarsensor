/****************************
*SonarSensor.h              *
*****************************
*author : Tugce Dinc        *
*IDE : Visual Studio 2015   *
*Last Update: 20/12/2018    *
*****************************/

#ifndef SonarSensor_h
#define SonarSensor_h
#include<iostream>
#include"PioneerRobotAPI.h"
#include"RangeSensor.h"
using namespace std;
/**
  *\author TUGCE DINC
  *\brief This class operates the sonar sensors' of a robot.
  */
class SonarSensor:public RangeSensor
{
public:
	/**
	  *\brief Constructor. "robotAPI" is initialized.
	  *\param robotAPi                      : new Robot
	  */
	SonarSensor(PioneerRobotAPI *robotAPi);
	/**
	  *\brief Destructor.
	  */
	~SonarSensor();
	/**
	  *\brief It returns the range of given index.
	  *\param index                         : given index.
	  *\return the range of given index.
	  */
	float getRange(int index);
	/**
	  *\brief It returns the maximum value among ranges. It assigns "index" the index of max value.  
	  *\param index                         : given index.
	  *\return Maximum value.
	  */ 
	float getMax(int& index);
	/**
	  *\brief It returns the minimum value among ranges. It assigns "index" the index of max value.  
	  *\param index                         : given index.
	  *\return Mininum value.
	  */ 
	float getMin(int&index);
	/**
	  *\brief It fills the given array("ranges") up by the sonar sensor ranges.  
	  *\param index                         : given array.
	  */ 
	void updateSensor(float ranges[]);
	/**
	  *\brief It returns the range of given index.
	  *\param index                         : given index.
	  *\return the range of given index.
	  */
	float operator[](int i);
	/**
	  *\brief It returns the angle of given index.
	  *\param index                         : given index.
	  *\return the angle of given index.
	  */
	float getAngle(int index);
	/**
	  *\brief It returns the min sensor range given angle interval.
	  *\param startAngle                    : start angle.
	  *\param endAngle                      : end angle.
	  *\param angle                         : The angle of minimum range.
	  *\return the angle of given index.
	  */
	float getClosestRange(float startAngle, float endAngle, float &angle);
};

#endif // !SonarSensor_h